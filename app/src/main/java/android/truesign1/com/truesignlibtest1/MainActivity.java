package android.truesign1.com.truesignlibtest1;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.nononsenseapps.filepicker.FilePickerActivity;
import com.truemd.android.lib.api.TrueSign;
import com.truemd.android.lib.api.TrueSignImpl;
import com.truemd.android.lib.exception.ConnectionException;
import com.truemd.android.lib.model.Document;
import com.truemd.android.lib.model.RequestSign;
import com.truemd.android.lib.model.RequestSignInput;
import com.truemd.android.lib.model.SignatoryInput;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    TextView textView, fileLocation;
    Button getFile, getSignRequest;
    Uri uri;
    ProgressDialog dialog;
    TrueSign api;

    private static final int FILE_CODE = 42;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dialog = new ProgressDialog(getApplicationContext());
        dialog.setMessage("Uploading File.");
        dialog.setTitle("Loading...");

        uri = null;
        textView = (TextView) findViewById(R.id.textView2);
        fileLocation = (TextView) findViewById(R.id.textView3);
        getFile = (Button) findViewById(R.id.button);
        getSignRequest = (Button) findViewById(R.id.button2);

        try {
            api =  TrueSignImpl.getApi("8b525c4f269b92f2f0ea12efed3a6767");
        } catch (ConnectionException e) {
            e.printStackTrace();
        }


        getFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // This always works
                Intent i = new Intent(getApplicationContext(), FilePickerActivity.class);
                // This works if you defined the intent filter
                // Intent i = new Intent(Intent.ACTION_GET_CONTENT);

                // Set these depending on your use case. These are the defaults.
                i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
                i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
                i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_FILE);

                // Configure initial directory by specifying a String.
                // You could specify a String like "/storage/emulated/0/", but that can
                // dangerous. Always use Android's API calls to get paths to the SD-card or
                // internal memory.
                i.putExtra(FilePickerActivity.EXTRA_START_PATH, Environment.getExternalStorageDirectory().getPath());

                startActivityForResult(i, FILE_CODE);
            }
        });

        getSignRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(uri != null) {
                    new GetRequestTask().execute(uri);
                }
                else
                    Toast.makeText(getApplicationContext(), "Choose file first.", Toast.LENGTH_SHORT).show();

            }
        });

        fileLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try
                {
                    if(uri != null) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(uri, "application/pdf");
                        startActivity(intent);
                    }
                    else
                        Toast.makeText(getApplicationContext(), "Choose file first.", Toast.LENGTH_SHORT).show();
                }
                catch(ActivityNotFoundException e)
                {
                    Toast.makeText(getApplicationContext(), "No software for PDF", Toast.LENGTH_SHORT).show();

                }

            }
        });


    }



    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FILE_CODE && resultCode == Activity.RESULT_OK) {
            if (data.getBooleanExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false)) {
                // For JellyBean and above
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    ClipData clip = data.getClipData();

                    if (clip != null) {
                        for (int i = 0; i < clip.getItemCount(); i++) {
                            uri = clip.getItemAt(i).getUri();
                            // Do something with the URI
                            fileLocation.setText(uri.toString());
                            Log.e("onActivityResult: ","1");


                        }
                    }
                    // For Ice Cream Sandwich
                } else {
                    ArrayList<String> paths = data.getStringArrayListExtra
                            (FilePickerActivity.EXTRA_PATHS);

                    if (paths != null) {
                        for (String path: paths) {
                            uri = Uri.parse(path);
                            // Do something with the URI
                            fileLocation.setText(uri.toString());
                            Log.e("onActivityResult: ","2");


                        }
                    }
                }

            } else {
                uri = data.getData();
                // Do something with the URI
                fileLocation.setText(uri.toString());
                Log.e("onActivityResult: ","3");




            }
        }
    }

    public class GetRequestTask extends AsyncTask<Uri, Void, String> {

        @Override
        protected void onPreExecute(){
            Log.e("Task","pre");
            //wizProgress.showNow();
        }


        @Override
        protected String doInBackground(Uri... params) {



            try {
                Log.e("Task","doinback"+ "upload started");

                Document document = api.createNewDocument(params[0]);
                Log.e("TrueSignAPI: "," upload: "+document.getId());

                Log.e("Task","doinback"+ "document uploaded");

                   RequestSignInput requestSignInput = new RequestSignInput();

                   ArrayList<SignatoryInput> signatoryInputs = new ArrayList<>();
                   signatoryInputs.add(new SignatoryInput("Yashvardhan Srivastava", "yash6992@gmail.com"));

                   requestSignInput.setPrepaid(true);
                   requestSignInput.setNotifications(true);
                   requestSignInput.setPurpose("Signing HR Documents");
                   requestSignInput.setRedirectUrl("http://www.google.com");
                   requestSignInput.setWebhook("http://www.google.com");
                   requestSignInput.setSignatoryInputs(signatoryInputs);

                   Log.e("Task", "doinback" + "request sign started");

                   RequestSign requestSignResponse = api.createNewRequestSign(requestSignInput, document);

                   return "" + requestSignResponse.getSignatories().get(0).getRequestUrl();


            } catch (Exception e) {
                e.printStackTrace();
                return "The file could not be uploaded.";
            }



        }

        @Override
        protected void onPostExecute(String content) {
            super.onPostExecute(content);
            //wizProgress.hideNow();
            Log.e("Task","post"+ content);
            textView.setText("The file can be signed at:\n"+content);

        }


    }


    void showDialog(){
        if (!dialog.isShowing()){
            dialog.show();
        }
    }
    void hideDialog(){
        if (dialog.isShowing()){
            dialog.dismiss();
        }
    }

}

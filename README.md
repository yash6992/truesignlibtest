# TrueSignLibTest
This is the android project to test the TrueSign android SDK.

Table of Contents
=================
* [Preface](#preface)
	* [Requester’s Workflow](#requester-overview)
	* [Signatory’s Workflow](#signatory-overview)
	* [Developer Overview](#developer-overview)
* [Requirements](#requirements)
* [Installation](#installation)
    * [Gradle](#gradle)
    * [AAR](#aar)
* [Authentication Keys](#authentication-keys)
* [End Points](#end-points)
    * [Test URLs](#test-urls)
    * [Production URLs](#production-urls)
* [TrueSign API](#payment-order-api)
    * [Create new Signing Request](#create-new-payment-order)
      * [Signing Request Creation Parameters](#payment-order-creation-parameters)
         
          


## Preface
Use our Java SDK for a quick setup with just 2-3 lines of code.
Start getting your documents e-signed from within your app by integrating our Java library with your application code.

### Requester’s Workflow
Once registration is complete, receiving/ signing documents via TrueSign is a simple 3 step process.

1.	  Upload the document to be signed. Previously uploaded documents are already on your dashboard.
2.	  State the purpose and who all need to sign the document.

  a.    only me.
  b.    me and others
  c.    only others

3.   The signatories receive a link to sign the document.

### Signatory’s Workflow
e-Signing on TrueSign is a 3 step process

1.   Signatory receives an online link for signing on his email.
2.   Signatory is handed over to TrueSign where they can preview the document to be signed. They now have to enter their 12-digit Aadhaar Number.
3.   Signatory enters the OTP they received on their registered mobile number or email. The document is signed and sent to the Requester’s dashboard.

### Developer Overview
e-Signing a document on TrueSign works by creating request URLs which can be sent to the required person to sign any document. Creating a Document is as simple as passing a file location. This step generates a Document object.
Once a Document is created, you can create a RequestSign (a signing request object) for it by specifying the purpose. This  generates a request URL which is used to sign or accept a sign in your mobile app, website or application.

You can choose to send the request URL directly to your signatories, who can then easily review and sign the document. Once signing is completed, you are updated on the dashboard.
## Requirements
Java Version : 1.7+ 
Android Studio : 2.0+
   
## Installation [ ![Download](https://api.bintray.com/packages/dev-accounts/maven/wrappers/images/download.svg) ](https://bintray.com/dev-accounts/maven/wrappers/_latestVersion) [![Build Status](https://travis-ci.org/Instamojo/instamojo-java.svg?branch=master)](https://travis-ci.org/Instamojo/instamojo-java)

### AAR

Download the truesignlibandroid.aar file from here.

### Gradle

You will need to add the following gradle dependancies to your app build.grade too.

```Java
compile group: 'org.apache.httpcomponents' , name: 'httpclient-android' , version: '4.3.5.1'
 compile group: 'org.apache.httpcomponents', name: 'httpcore', version: '4.4.4'
 compile group: 'commons-codec', name: 'commons-codec', version: '1.9'
 compile group: 'commons-logging', name: 'commons-logging', version: '1.2'

compile group: 'com.google.code.gson', name: 'gson', version: '2.6.2'
 compile('org.apache.httpcomponents:httpmime:4.3') {     exclude module: "httpclient" }

```

Or, you can download and include the following jar files and include their licenses in your project.

1. [Apache HttpClient v4.5.2](http://mvnrepository.com/artifact/org.apache.httpcomponents/httpclient/4.5.2)
2. [Apache HttpCore v4.4.4](http://mvnrepository.com/artifact/org.apache.httpcomponents/httpcore/4.4.4)
3. [Apache Commons Codec v1.9](http://mvnrepository.com/artifact/commons-codec/commons-codec/1.9)
4. [Apache Commons Logging v1.2](http://mvnrepository.com/artifact/commons-logging/commons-logging/1.2)
5. [Gson v2.6.2](http://mvnrepository.com/artifact/com.google.code.gson/gson/2.6.2)


## Authentication Keys
Generate CLIENT_ID and CLIENT_SECRET for specific environments from the following links.

[Test Environment](https://truesign.co/developers)

## End Points
### Test URLs
auth_endpoint : http://truesign-carebook.rhcloud.com/api/v2/

endpoint: http://truesign-carebook.rhcloud.com/api/v2/

### Production URLs
auth endpoint : http://truesign-carebook.rhcloud.com/api/v2/

endpoint: http://truesign-carebook.rhcloud.com/api/v2/

## TrueSign API
### Create new TrueSign API Object.
```Java
TrueSign api = null;

try {     api =  TrueSignImpl.getApi(<Client-API-Key>); } catch (ConnectionException e) {     e.printStackTrace(); }
	
```
### Upload Document to be signed.
Get the Uri for the file to be uploaded using a File Picker. You can use the file picker used in the demo android project. Uri uri is the location URL for the PDF File in the local storage.
```Java
Document document = api.createNewDocument(uri); 
	
```

### Signing Request Creation Parameters

```Java
RequestSignInput requestSignInput = new RequestSignInput();

ArrayList<SignatoryInput> signatoryInputs = new ArrayList<>();
signatoryInputs.add(new SignatoryInput("Yashvardhan Srivastava", "yash6992@gmail.com"));
requestSignInput.setPrepaid(true);
requestSignInput.setNotifications(true);
requestSignInput.setPurpose("Signing HR Documents");
requestSignInput.setRedirectUrl("http://www.google.com");
requestSignInput.setWebhook("http://www.google.com");
requestSignInput.setSignatoryInputs(signatoryInputs);

RequestSign requestSignResponse = api.createNewRequestSign(requestSignInput, document);

ArrayList<String> requestURLs = new ArrayList<>();

for(Signatory signatory : requestSignResponse.getSignatories)
{
	String requestURL = signatory.getRequestUrl();
	requestURLs.add(requestURL);
}

```
#### Request Object fields
prepaid:	boolean : true
If the payment for the signature is to be deducted from the signature credits or to be paid by the signatory.

purpose:	String : required
The purpose of signing the document.

redirect_url:String
The url to which the signatory is to redirected to after successful signature.

webhook:	String
The url to which signature details are to be sent.

signatories:JSONArray : required
The list of signatories. Their name and email addresses.

notifications:boolean : true
The file to uploaded for signing.
##### Signatories ArrayList fields
name: String : required
The name of the signatory.

email: String : required
The email of the signatory. The signing request URL will be sent to this email id if the notifications is true.


